-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 07, 2021 lúc 12:25 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `vue_laravel`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `brands`
--

CREATE TABLE `brands` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `brands`
--

INSERT INTO `brands` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Supere', 'Supreme được biết đến là một thương hiệu thời trang đường phố của Mỹ vô cùng nổi tiếng, với mức độ tiêu thụ khủng trên toàn cầu.', 1, NULL, NULL),
(3, 'A Bathing Abe', 'A Bathing Abe thương hiệu lâu đời', 1, NULL, NULL),
(4, 'The Hundres', 'Tại Việt Nam, The Hundreds không phải là một cái tên nổi bật, bởi họ không thuộc dàn thương hiệu \"hypebeast\"', 1, NULL, NULL),
(5, 'Alife', 'Thương hiệu đến từ New York là Alife đã trở lại với một cái bắt tay cùng với thương hiệu Crocs.', 0, NULL, NULL),
(6, 'Neighborhood', 'Neighborhood  thương hiệu ngoại nhập', 1, NULL, NULL),
(7, 'CLOT', 'CLOT thương hiệu lâu đời', 0, NULL, NULL),
(8, 'Apabulo Cold', 'Apabulo Cold thương hiệu', 1, NULL, NULL),
(9, 'Fibecrop', 'Thương hiệu Fibecrop', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Jacket', '1629626117_product-5.jpg', NULL, NULL),
(2, 'Men & Women', '1629626096_product-6.jpg', NULL, NULL),
(3, 'T - Shirt', '1633495238_product-13.jpg', NULL, NULL),
(4, 'Accessories', '1629626560_product-1.jpg', NULL, NULL),
(6, 'Coat', '1629626794_ao-khoac.jpg', NULL, NULL),
(11, 'Polo T-Shirt', '1630593367_polo.jpg', NULL, NULL),
(12, 'V Neck T-shirts', '1630593590_v neck.jpg', NULL, NULL),
(13, 'Hooded T-Shirts', '1630593765_hooded.jpg', NULL, NULL),
(14, 'Footwear', '1630593873_footwear.jpg', NULL, NULL),
(16, 'Men & Women 2', '1631982634_Ao-khoac-len.jpg', NULL, NULL),
(17, 'Fender 8800', '1631982662_len.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category_blogs`
--

CREATE TABLE `category_blogs` (
  `id` int(11) NOT NULL,
  `category_blog_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_blog_slug` text COLLATE utf8_unicode_ci NOT NULL,
  `category_blog_des` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category_blogs`
--

INSERT INTO `category_blogs` (`id`, `category_blog_name`, `category_blog_slug`, `category_blog_des`) VALUES
(1, 'Đời Sống Tuổi Trẻ', 'doi-song-tuoi-tre', 'Cuộc sống ngày càng hiện đại'),
(2, 'Làm đẹp', 'lam-dep', 'Làm đẹp cuộc sống');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id_customer` bigint(20) UNSIGNED NOT NULL,
  `name_customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id_customer`, `name_customer`, `phone_customer`, `address_customer`, `email_customer`, `city_customer`, `image_customer`, `created_at`, `updated_at`) VALUES
(1, 'Ngô Kim Hoàng Minh', '0941314137', '162 Đống Đa, Đà Nẵng', 'hoangminhcp10@gmail.com', 'Đà Nẵng', '1629640550_ad4.jpg', NULL, NULL),
(2, 'Trương Quân Bảo', '0965214387', '234 Thanh Sơn', 'quanbao@gmail.com', 'Đà Nẵng', '1629639521_jisoo.jpg', NULL, NULL),
(3, 'Hoàng Thùy Linh', '0854123654', '23 Y Jut, Pleiku', 'hoangthuylinh@gmail.com', 'Gia Lai', '1629644138_ad2.jpg', NULL, NULL),
(4, 'Cao Thái Trang', '0852631478', 'Cầu Giấy, Hà Nội', 'caothaitrang@gmail.com', 'Hà Nội', '1629644217_ad1.jpg', NULL, NULL),
(5, 'Nguyễn Mạnh Hùng', '0958263417', '206 Hùng vương, tx Buôn Hồ', 'nguyenmanhhung@gmail.com', 'ĐakLak', '1629644340_ad5.jpg', NULL, NULL),
(6, 'Nguyễn Trọng Đạt', '0874963512', '54 Thủy Lan, Thủ Dầu 1', 'nguyentrongdat@gmail.com', 'Bình Dương', '1629644527_ad3.jpg', NULL, NULL),
(7, 'Chu Mỹ', '0985741236', '34 Thanh Thủy, Đà Nẵng', 'chumy@gmail.com', 'Đà Nẵng', '1629644604_Ao-khoac-len.jpg', NULL, NULL),
(8, 'Trình Uyển Nhi', '0985632741', 'Phan Thiết', 'trinhuyennhi@gmail.com', 'Phan Thiết', '1629645454_ad8.jpg', NULL, NULL),
(9, 'Nguyễn Minh Châu', '0859632417', 'Thanh Hóa', 'nguyenminhchau@gmail.com', 'Thanh Hóa', '1629645503_ad7.jpg', NULL, NULL),
(10, 'Nguyễn Thanh Tùng', '0852631749', 'Hải Dương', 'nguyenthanhtung@gmail.com', 'Hải Dương', '1629645562_ad9.jpg', NULL, NULL),
(11, 'Nguyễn Mỹ Thơ', '0852631978', 'Cần Thơ', 'nguyenmytho@gmail.com', 'Cần Thơ', '1629645654_ad10.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2021_07_02_164915_customer', 1),
(10, '2021_07_07_155220_create_categories_table', 1),
(11, '2021_07_22_105250_create_products_table', 1),
(12, '2021_09_01_074714_create_brands_table', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0608f620b3cf867966231b38e45c0835b1275d3a548aebfda64c432034b6e939582e8f06f2957f8e', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-26 10:17:27', '2021-09-26 10:17:27', '2022-09-26 17:17:27'),
('07e366122ee2d36d40927d0f2d9b7cb72f1495c9971747ae08533a635663312626aaf947d82cb4c8', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 00:54:58', '2021-09-18 00:54:58', '2022-09-18 07:54:58'),
('0af3ed829e4f6376ebcef7f3001bc4489ddb1ae12c1de6e4bc886e0a8c81a65faebaf778954220b1', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 08:02:18', '2021-09-18 08:02:18', '2022-09-18 15:02:18'),
('0ce94028e473ad48d423e1f9fa2fe2fb847708e31e90b2fbf395cdb2cc8afbece5a31928954b4af8', 2, 1, 'hoangnam@gmail.com', '[]', 1, '2021-09-18 06:34:59', '2021-09-18 06:34:59', '2022-09-18 13:34:59'),
('1266172cfcccb3ccc3254af937d910f933d04997b73f2be30f3ba95725864882bfdf3828572fa0f8', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 07:45:06', '2021-09-18 07:45:06', '2022-09-18 14:45:06'),
('175f013397ee3b20bd8b1d3b47f81eff17b34988db5429eeb186a3b82b1b3fb59c1a851ff328a2a7', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 06:30:46', '2021-09-18 06:30:46', '2022-09-18 13:30:46'),
('1bcee7dbe76f3787de3d4543526ea62083408576bebdebfdc0977827f757514bf6668e037de86319', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-08-22 03:45:09', '2021-08-22 03:45:09', '2022-08-22 10:45:09'),
('1d4ba8e133410bf2ca656b906a9ee74e49d1cc2856a5cfacf7b46a8abc34a876f3694462d09a2335', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-10-05 21:40:14', '2021-10-05 21:40:14', '2022-10-06 04:40:14'),
('1dc8a1974601339e9c9e62eb04aefedef00f8c22ee239b589a477a02d269ad0a718576a783d1f3a7', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 01:38:58', '2021-09-18 01:38:58', '2022-09-18 08:38:58'),
('315e6a932f0e37aba2c388f24ee483b17eb04b2107e25590125e9214a95d7b9f90e920290547eadf', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 08:53:05', '2021-09-18 08:53:05', '2022-09-18 15:53:05'),
('32de7ff3180fd704461ba36dd681e61b7a7083804008918f3c3f2384b5c44ab4b6f10c9529bc25a4', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 07:47:29', '2021-09-18 07:47:29', '2022-09-18 14:47:29'),
('357f4a4cd65c734fb4b53ecff4a2e2dac81d973346d8a0cadd6185f1febbd2f893775ac1fb50a5ec', 2, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 03:29:01', '2021-09-18 03:29:01', '2022-09-18 10:29:01'),
('36fff9b06fc98c6cbe1582cf8ce265cff736a48382d549cf339671329bcd880551efa48c29731189', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 09:23:55', '2021-09-18 09:23:55', '2022-09-18 16:23:55'),
('3c86a9369dbb7c3ba7ad95f1e61614f5098588d61732cf46ea1582e3ec774b9de5c50cbfdd2e4742', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-06 07:19:56', '2021-09-06 07:19:56', '2022-09-06 14:19:56'),
('4a85a8cb468ca2d9748a97f10ac20b71bf7c41b07a17a6e2aeeec90ddeb9eac621eb730bb05d5fe1', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-27 08:54:24', '2021-09-27 08:54:24', '2022-09-27 15:54:24'),
('4b727358625b6bf1c82c796a25ad6072b6289bcfc73d2dae3319af4c14c9d7089ecf7bac68793145', 1, 1, 'trungthong', '[]', 0, '2021-09-06 09:18:29', '2021-09-06 09:18:29', '2022-09-06 16:18:29'),
('4ffcc7a571503aed24dc8ce4534ce8332f34183c22f6914cff2c3f78ccf8fb77b16e456f8c0a53d3', 2, 1, 'hoangnam@gmail.com', '[]', 0, '2021-09-18 03:41:21', '2021-09-18 03:41:21', '2022-09-18 10:41:21'),
('57b83a1b090af6c8dbdb7b3b1a64a38624629e44abf968b385950e371ace54bc258409a01a82e4f8', 2, 1, 'hoangnam@gmail.com', '[]', 0, '2021-09-18 08:44:26', '2021-09-18 08:44:26', '2022-09-18 15:44:26'),
('5d62f8e2026d04cefc753cea3b110178f530d8b844c2588df0ca720ae124c39330a133c43d01fcb1', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-10-05 01:57:42', '2021-10-05 01:57:42', '2022-10-05 08:57:42'),
('5dda4b07880f59c5db24861bf6cba9868775e7eeffc52d349af779993983b14403482b4d5de53395', 1, 1, 'hoangminhcp10@gmail.com', '[]', 1, '2021-09-18 06:32:58', '2021-09-18 06:32:58', '2022-09-18 13:32:58'),
('6364301ae234b29728dad9b9cebf6ce93af921d6a26e93d91fb16368f878d9b2108eee1310c9d7d2', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 05:28:33', '2021-09-18 05:28:33', '2022-09-18 12:28:33'),
('66c8e6e824ffb0c53305b8c9e8a981e37ee1d4a6df4cdde988c48d1a50772a28b582256363982600', 2, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 03:29:35', '2021-09-18 03:29:35', '2022-09-18 10:29:35'),
('6780ff03d816201be325f6081d39ce7356f1c6654a1bf52cfb04bc20ac7a407461a77911b1eca5de', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-27 07:48:02', '2021-09-27 07:48:02', '2022-09-27 14:48:02'),
('67e4ec9ebbfbbfc1f619ec5491e4ecc3a3254fce9f5ec12dcd3b9c783ce8a10805701224d470dd18', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 06:56:58', '2021-09-18 06:56:58', '2022-09-18 13:56:58'),
('6b1fb7d3abc72604c17cfeb274939e5e56b6f552f39d86ef8b274b03544b2240002decfdf6904d96', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 03:25:04', '2021-09-18 03:25:04', '2022-09-18 10:25:04'),
('77dcbdc5573225bbb66303a65aefd3108fa5ecc1bf36b37a75581ba90010e5e4bb6a9185e57a2311', 1, 1, 'trungthong', '[]', 0, '2021-09-06 09:18:16', '2021-09-06 09:18:16', '2022-09-06 16:18:16'),
('7b83762dc05db04636e890c70e6b17bb788231d5b82dec23db2de764dd615a1a8e442f1d5fc6b85e', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 05:38:47', '2021-09-18 05:38:47', '2022-09-18 12:38:47'),
('7b87a13733829d5ac503ad6f020c0717eb43c8396e622071f78ae049b5a578ce63fda15301767140', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-27 08:57:28', '2021-09-27 08:57:28', '2022-09-27 15:57:28'),
('7fc3f3e8804a7dceef733f990510c51a0b5d8d0157d6d3ef211b5e848a63335a8f2a6b1eeda54f07', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 03:05:51', '2021-09-18 03:05:51', '2022-09-18 10:05:51'),
('8b258df9a9149a4788976503971631ff9b9569a9ed46f0f883eed389602f173fcdb7dc34788793c7', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 07:46:26', '2021-09-18 07:46:26', '2022-09-18 14:46:26'),
('8e4280f091ff957d19b0474e13ea7edef9585c9c802d7481ddb5880a73909a5e68a95fab65ed28ff', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 07:03:33', '2021-09-18 07:03:33', '2022-09-18 14:03:33'),
('951bc545d8ae117424832207306e094249d4d4c135fe89c10bdefdde35afd4714c44a78439d81a3b', 1, 1, 'trungthong', '[]', 0, '2021-09-06 09:31:37', '2021-09-06 09:31:37', '2022-09-06 16:31:37'),
('976e5e83e9e9f99a342b9b832a2d86d90b787ee765d7de391ccd9d2923580677979274b4f9b3e88a', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 03:20:57', '2021-09-18 03:20:57', '2022-09-18 10:20:57'),
('acfe736d8f02abeae8150ca2503091f475a4ec023cf6617990006144467612823f4f2f87db49ef80', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-04 01:15:33', '2021-09-04 01:15:33', '2022-09-04 08:15:33'),
('adc2ac19c9f4cfcb1eea45bea0d5ced11db3f793e56eb03dbf237580f1efbde2eaceb9b776e61068', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 06:02:03', '2021-09-18 06:02:03', '2022-09-18 13:02:03'),
('af7a8f1a7fdd92ab8a032f7598a1296b8076de6833ede65813b7c0218da612235c9e78ef0d3a3a46', 2, 1, 'hoangnam@gmail.com', '[]', 0, '2021-09-27 08:58:00', '2021-09-27 08:58:00', '2022-09-27 15:58:00'),
('b6379e565f7e6fdab28b0eada794c0f7f457fb9e2546e6b239f697db70b2deb4cf89b3e2a178ddc4', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 07:37:28', '2021-09-18 07:37:28', '2022-09-18 14:37:28'),
('b88ce0f524da08ad97fee015cf0024426ec30419a15b89aff4bea7f57bb9783a670c973d591ccb61', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-27 08:58:40', '2021-09-27 08:58:40', '2022-09-27 15:58:40'),
('bbd814c48324ec0c91372e61753e8735b12d81eea86a64f5242db26df4282c2bd1a278fde7f3b06b', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 06:37:00', '2021-09-18 06:37:00', '2022-09-18 13:37:00'),
('bc09f0eefc675327c7ba9193c917e2c8d131b80cb24fb041e6af3fe841d2626b51c8ae89769d9b3a', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 03:27:50', '2021-09-18 03:27:50', '2022-09-18 10:27:50'),
('bdc2643d832b572ca467b3d6815c5a1294078e8cd8dc2746c201ea8c6aea3501147a2db470bd20fb', 2, 1, 'hoangnam@gmail.com', '[]', 0, '2021-09-18 08:04:43', '2021-09-18 08:04:43', '2022-09-18 15:04:43'),
('c0650dcd30c931e53e0b0317267ee69249c109e7c7b872fe8e584094a0233b21d0629db51d7b4735', 2, 1, 'hoangnam@gmail.com', '[]', 1, '2021-09-18 07:04:15', '2021-09-18 07:04:15', '2022-09-18 14:04:15'),
('c28fdd14115d7223c7947c719a33160c96579d7d9dee5ea13ea1dd74c4437616ed65026b12532927', 4, 1, 'thanhbinh@gmail.com', '[]', 0, '2021-09-18 08:20:21', '2021-09-18 08:20:21', '2022-09-18 15:20:21'),
('d31a072ce969fd98f7dba730f382f6fbb1cb90743f8d0afcf30a1fafa1b1d82365143a37b6cf1a54', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-26 08:47:18', '2021-09-26 08:47:18', '2022-09-26 15:47:18'),
('dc2bb462968268a0ae186e8b2c136ca22d7ee6c0026030121589aff567d5839d578b49e8ae6bf56b', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 08:12:33', '2021-09-18 08:12:33', '2022-09-18 15:12:33'),
('dcb6037fce97aa0fb8fefc745b8de2dd52c406b0bd8507222d44c62d1cc6c9eee00794d815137614', 1, 1, 'trungthong', '[]', 0, '2021-09-06 09:29:03', '2021-09-06 09:29:03', '2022-09-06 16:29:03'),
('df53c9b497966d8111b851bd85fd2cb6739cd516efd2f23803c089e52e2a213ddc4060159577126f', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 06:44:47', '2021-09-18 06:44:47', '2022-09-18 13:44:47'),
('e016fb3a4e375602534b8a989fc6e167a6a18bd7a7a8f61af08c7124d3d7fa02be77fb6eec670405', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 08:36:33', '2021-09-18 08:36:33', '2022-09-18 15:36:33'),
('ecf581bc2d79c86e226f7d320d39ca47f0cf5569acbca5b192f047abadec3452000867d03ed74b8e', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-26 08:47:15', '2021-09-26 08:47:15', '2022-09-26 15:47:15'),
('f1ffb7be2cdcb1eec8763cf12b903863569136a294d783a4259b89fb34f054a3b5668e32d58240bf', 2, 1, 'hoangnam@gmail.com', '[]', 0, '2021-09-18 08:56:39', '2021-09-18 08:56:39', '2022-09-18 15:56:39'),
('f4023413afef5fdc3c4deda8d6609c815e353c111fc97ef68521f0cbeb51ce6201d4cb1c89c8f28c', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 03:44:30', '2021-09-18 03:44:30', '2022-09-18 10:44:30'),
('fd82562ebdcd6dbe206d8036b93a847e31689a3dc73c94581b2d002ade788fd2f73a1b90f01ca691', 1, 1, 'Personal Access Tokens', '[\"adminstrator\"]', 0, '2021-09-18 03:23:29', '2021-09-18 03:23:29', '2022-09-18 10:23:29'),
('fe774263e1f015f1f4ed7e18fa1c615c53368d3528b9e6cf3d12da0481f81f170a62767988205080', 1, 1, 'hoangminhcp10@gmail.com', '[]', 0, '2021-09-18 05:33:59', '2021-09-18 05:33:59', '2022-09-18 12:33:59');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'GwXAI3dIiO3X2t7ZlnqDcPgqpglR0DWwoF4af10I', NULL, 'http://localhost', 1, 0, 0, '2021-08-22 03:42:48', '2021-08-22 03:42:48'),
(2, NULL, 'Laravel Password Grant Client', 'cKn4bQz6JBOGW00o017kuOxmhNDimxrdvJQ4q3HN', 'users', 'http://localhost', 0, 1, 0, '2021-08-22 03:42:48', '2021-08-22 03:42:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-08-22 03:42:48', '2021-08-22 03:42:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `name`, `image`, `product_des`, `price`, `discount`, `product_status`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'Áo khoác len nữ  hồng', '1629627305_len.jpg', '<p><em>H&agrave;ng ngoại nhập, len lấy từ những ch&uacute; cừu</em></p>', '250000', '220000', 1, NULL, NULL),
(2, 6, 3, 'Áo khoác da nam', '1629627439_ao-khoac.jpg', '<p><em>Mẫu s&eacute;t bộ quần &aacute;o kho&aacute;c thể thao nam d&agrave;i tay h&egrave; thu phong c&aacute;ch H&agrave;n Quốc m&atilde; KHOAC.</em></p>', '500000', '350000', 1, NULL, NULL),
(3, 3, 1, 'Áo thun unisex', '1629627546_product-11.jpg', '<p><em>&Aacute;o thun form rộng, đủ mọi loại sizes.</em></p>', '150000', '120000', 1, NULL, NULL),
(4, 3, 3, 'Áo thun nam nâu', '1629627972_men.jpg', '<p>H&agrave;ng chất lượng, &ocirc;m cơ thể, co d&atilde;n</p>', '100000', '89000', 0, NULL, NULL),
(5, 2, 4, 'Đầm dự tiệc', '1629628022_product-12.jpg', '<p>Đầm nữ t&iacute;nh</p>', '558000', '540000', 1, NULL, NULL),
(6, 1, 4, 'Áo Khoác Bomber Đen 8K32', '1630592684_8K32-554x800.png', '<p>Miễn ph&iacute; giao h&agrave;ng chỉ &aacute;p dụng cho đơn h&agrave;ng từ 449.000 VND với c&aacute;c sản phẩm được cung cấp bởi CELEB</p>\n\n<p>CELEB chỉ hỗ trợ giao h&agrave;ng trong giờ l&agrave;m việc (9:00-18:00) từ Thứ 2 &ndash; Thứ 6 v&agrave; (9:00-12:00) Thứ 7.</p>', '426000', '387000', 1, NULL, NULL),
(7, 1, 5, 'Tacvasen Quân Áo Khoác Nam', '1630592942_Tacvasen-Qu-n-o-Kho-c-Nam-M-a-ng-H-p-D-nh-o-Kho.jpg', '<p>1. chiếc &aacute;o kho&aacute;c n&agrave;y k&iacute;ch thước Ch&acirc;u &Aacute;, vui l&ograve;ng chọn 2-3 k&iacute;ch thước lớn hơn so với b&igrave;nh thường của bạn khi mặc.(Chiều cao v&agrave; trọng lượng tham khảo: 175cm,75kg,2XL L&agrave; Vừa).<br />\n2. K&iacute;ch thước ch&acirc;u &Aacute; l&agrave; 2-3 K&iacute;ch cỡ nhỏ hơn so với MỸ/ANH/RU/EU/K&Iacute;CH THƯỚC.<br />\n3. Xin vui l&ograve;ng cho ph&eacute;p m&agrave;u hợp l&yacute; kh&aacute;c biệt do c&aacute; nh&acirc;n M&agrave;n h&igrave;nh m&aacute;y t&iacute;nh.</p>', '1250000', '1150000', 0, NULL, NULL),
(8, 1, 6, 'ÁO KHOÁC KAKI HỘP 4 TÚI', '1630593216_80cde76f01cfe391bade-2751.jpg', '<p>Chất liệu :KAKI 2 lớp cao cấp, kh&ocirc;ng x&ugrave; l&ocirc;ng, kh&ocirc;ng nhăn, đường may đẹp.lớp l&oacute;t d&ugrave; trong. KAKI co gi&atilde;n, d&agrave;y dặn, h&agrave;ng xuất sắc, độ tươi trẻ ...</p>', '180000', '140000', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `verification_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `verification_code`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hoàng Minh', 'hoangminhcp10@gmail.com', NULL, '$2y$10$Qjdq76UbUD5fmqSTXcu0AutHrEzRPcPqp3L3ClBaWFbbVI.a5hMJS', 'adminstrator', NULL, NULL, '2021-08-22 03:39:49', '2021-08-22 03:39:49'),
(2, 'HoangNam', 'hoangnam@gmail.com', NULL, '$2y$10$Qjdq76UbUD5fmqSTXcu0AutHrEzRPcPqp3L3ClBaWFbbVI.a5hMJS', 'manager', NULL, NULL, NULL, NULL),
(4, 'Thanh Binh', 'thanhbinh@gmail.com', NULL, '$2y$10$iZMZNXByZdl/cH8puG9/1.h8TjxJPY8yxcMMIZPvkkNuDRwQegGXq', 'customer', NULL, NULL, '2021-09-18 08:19:36', '2021-09-18 08:19:36');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category_blogs`
--
ALTER TABLE `category_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `brand_id` (`brand_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `category_blogs`
--
ALTER TABLE `category_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
